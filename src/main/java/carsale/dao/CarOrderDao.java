package carsale.dao;



/**
 * Created by Busik on 1/16/17.
 */

import carsale.domain.CarOrder;
import java.util.List;

/** Объект для управления персистентным состоянием объекта CarOrder */
public interface CarOrderDao{
    /** Возвращает объект соответствующий записи с первичным ключом key или null */
    public CarOrder read(int key) throws DAOException;
    /** Добавляет объект carOrder в базу данных */
    public void insert (CarOrder carOrder) throws DAOException;
    /** Сохраняет состояние объекта carOrder в базе данных */
    public void update(CarOrder carOrder) throws DAOException;

    /** Удаляет запись об объекте из базы данных */
    public void delete(CarOrder carOrder) throws DAOException;

    /** Возвращает список объектов соответствующих всем записям в базе данных */
    public List<CarOrder> getAll()throws DAOException;
    /** Удаляет все стейтментыперед закрытием приложения */
    public void close()throws DAOException;
}
