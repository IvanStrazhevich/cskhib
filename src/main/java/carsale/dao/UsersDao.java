package carsale.dao;

import java.util.List;
import carsale.domain.Users;

/**
 * Created by Busik on 3/15/17.
 */
public interface UsersDao {


    /** Возвращает список объектов соответствующих всем записям в базе данных */
    public List<Users> getAll() throws DAOException;
    /** Добавляет объект соответствующий записи в базу данных */
    public void insert(Users user) throws DAOException;
    /** Удаляет запись об объекте из базы данных */
    public void delete(Users user) throws DAOException;
    public void close() throws DAOException;

}
