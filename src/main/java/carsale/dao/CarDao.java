package carsale.dao;

/**
 * Created by Busik on 12/30/16.
 */

import carsale.domain.Car;
import java.util.List;

/** Объект для управления персистентным состоянием объекта Car */
public interface CarDao{

    /** Возвращает объект соответствующий записи с первичным ключом key или null */
    public Car read(int key)throws DAOException;
    /** Сохраняет объект в базу данных */
    public void insert(Car car) throws DAOException;

    /** Сохраняет состояние объекта group в базе данных */
    public void update(Car car) throws DAOException;

    /** Удаляет запись об объекте из базы данных */
    public void delete(Car car) throws DAOException;

    /** Возвращает список объектов соответствующих всем записям в базе данных */
    public List<Car> getAll() throws DAOException;
    public void close()throws DAOException;
}