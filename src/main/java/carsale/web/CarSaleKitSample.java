package carsale.web;
import carsale.dao.DAOException;
import carsale.domain.Car;
import carsale.domain.CarOrder;
import carsale.domain.Manufacture;
import carsale.domain.Users;
import carsale.sql.MySQLCarDao;
import carsale.sql.MySQLCarOrderDao;
import carsale.sql.MySQLManufactureDao;
import carsale.sql.MySQLUsersDao;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by Busik on 2/15/17.
 */
public class CarSaleKitSample extends HttpServlet {


    protected boolean checkInputCar(HttpServletRequest req, HttpServletResponse resp) throws DAOException {
        try {
            Integer.parseInt(req.getParameter("Car_Brand"));
            Integer.parseInt(req.getParameter("Power"));
            Integer.parseInt(req.getParameter("Range"));
            Integer.parseInt(req.getParameter("Price"));
            Integer.parseInt(req.getParameter("Quantity"));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } if(req.getParameter("Car_Brand").toString().equals("")||
                req.getParameter("Power").toString().equals("")||
                req.getParameter("Range").toString().equals("")||
                req.getParameter("Price").toString().equals("")||
                req.getParameter("Quantity").toString().equals("")||
                req.getParameter("Model").toString().equals("")) {
            return false;
        } else {
                return true;
        }

    }
    protected boolean checkInputEditCar(HttpServletRequest req, HttpServletResponse resp) throws DAOException {
        try {
            req.getParameter("Car_Brand");
            Integer.parseInt(req.getParameter("Power"));
            Integer.parseInt(req.getParameter("Range"));
            Integer.parseInt(req.getParameter("Price"));
            Integer.parseInt(req.getParameter("Quantity"));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } if(req.getParameter("Car_Brand").toString().equals("")||
                req.getParameter("Power").toString().equals("")||
                req.getParameter("Range").toString().equals("")||
                req.getParameter("Price").toString().equals("")||
                req.getParameter("Quantity").toString().equals("")||
                req.getParameter("Model").toString().equals("")) {
            return false;
        } else {
            return true;
        }

    }
    protected boolean checkInputCarOrder(HttpServletRequest req, HttpServletResponse resp) throws DAOException {
        try {
            if(req.getParameter("First_Name").equals("")||req.getParameter("Second_Name").equals("")){
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } return false;

    }
    protected boolean checkInputManufacture(HttpServletRequest req, HttpServletResponse resp) throws DAOException {
        try {
            if(req.getParameter("carBrand").equals("")){
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } return false;

    }
    protected boolean checkInputUser(HttpServletRequest req, HttpServletResponse resp) throws DAOException {
        try {
            if(req.getParameter("loginJSP").equals("")||req.getParameter("passJSP").equals("")){
            } else {
                return true;
                }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } return false;

    }

    protected void addCarOrder(HttpServletRequest req, HttpServletResponse resp) throws DAOException {
        try {
            MySQLCarDao carDao = (MySQLCarDao) req.getSession().getAttribute("SQLCar");
            MySQLCarOrderDao carOrderDao = (MySQLCarOrderDao) req.getSession().getAttribute("SQLCarOrder");
            Car car;
            CarOrder carOrder = new CarOrder();
            car = carDao.read(Integer.parseInt(req.getParameter("id")));
            carOrder.setCarId(car.getId());
            carOrder.setManufactureId(car.getManufactureId());
            carOrder.setFirstName(req.getParameter("First_Name"));
            carOrder.setSecondName(req.getParameter("Second_Name"));
            car.setQuantity((car.getQuantity()-1));
            carDao.update(car);
            carOrderDao.insert(carOrder);
        } catch (Exception e) {
            throw new DAOException (e);
        }
    }
    protected void removeCarOrder(HttpServletRequest req, HttpServletResponse resp) throws DAOException {
        try {
            MySQLCarOrderDao carOrderDao = (MySQLCarOrderDao) req.getSession().getAttribute("SQLCarOrder");
            CarOrder carOrder = new CarOrder();
            carOrder.setId(Integer.parseInt(req.getParameter("id")));
            carOrderDao.delete(carOrder);
        } catch (Exception e) {
            throw new DAOException (e);
        }
    }
    protected List getListCarOrder (HttpServletRequest req, HttpServletResponse resp) throws DAOException {
        List<CarOrder> list;
        try {
            MySQLCarOrderDao carOrderDao = (MySQLCarOrderDao) req.getSession().getAttribute("SQLCarOrder");
            list = carOrderDao.getAll();

        } catch (Exception e) {
            throw new DAOException (e);
        } return list;
    }
    protected void fillFormCarOrders (HttpServletRequest req, HttpServletResponse resp) throws DAOException, ServletException, IOException {
        try {
            if (req.getParameter("CarOrderList") == null) {
                List<CarOrder> orderList = new ArrayList();
                orderList = getListCarOrder(req, resp);
                req.setAttribute("CarOrderList", orderList);
                if(req.getParameter("CarList") == null){
                    List<Car> carList = new ArrayList();
                    carList = getListCar(req, resp);
                    req.setAttribute("CarList", carList);
                }
            }

        } catch (Exception e) {
            throw new DAOException(e);
        }
        if (req.getRequestDispatcher("/jsp/CarOrder.jsp") != null) {
            req.getRequestDispatcher("/jsp/CarOrder.jsp").forward(req, resp);
        }
    }
    protected void removeManufacture(HttpServletRequest req, HttpServletResponse resp) throws DAOException {
        try {
            Manufacture m = new Manufacture();
            m.setId(Integer.parseInt(req.getParameter("id")));
            MySQLManufactureDao manuDao = (MySQLManufactureDao) req.getSession().getAttribute("SQLManufacture");
            manuDao.delete(m);
        } catch (Exception e) {
            throw new DAOException (e);
        }

    }
    protected void addManufacture(HttpServletRequest req, HttpServletResponse resp) throws DAOException {
        try {
            Manufacture m = new Manufacture();
            m.setCarBrand(req.getParameter("carBrand"));
            MySQLManufactureDao manuDao = (MySQLManufactureDao) req.getSession().getAttribute("SQLManufacture");
            manuDao.insert(m);
        } catch (Exception e) {
            throw new DAOException (e);
        }

    }
    protected List getListManufacture(HttpServletRequest req, HttpServletResponse resp) throws DAOException {
        List<Manufacture> list;
        try {
            MySQLManufactureDao manuDao = (MySQLManufactureDao) req.getSession().getAttribute("SQLManufacture");
            list = manuDao.getAll();
        } catch (Exception e) {
            throw new DAOException (e);
        } return list;
    }
    protected void showManufactures (HttpServletRequest req, HttpServletResponse resp) throws DAOException, ServletException, IOException {
        try {
            if (req.getParameter("ManufactureList") == null) {
                List<Manufacture> list = new ArrayList();
                list = getListManufacture(req, resp);
                req.setAttribute("ManufactureList", list);
            }
        } catch (Exception e) {
            throw new DAOException(e);
        }
        if (req.getRequestDispatcher("/jsp/Manufacture.jsp") != null) {
            req.getRequestDispatcher("/jsp/Manufacture.jsp").forward(req, resp);
        }
    }
    protected void addCar(HttpServletRequest req, HttpServletResponse resp) throws DAOException {
        try {
            MySQLCarDao carDao = (MySQLCarDao) req.getSession().getAttribute("SQLCar");
            MySQLManufactureDao manufactureDao = (MySQLManufactureDao) req.getSession().getAttribute("SQLManufacture");
            Car car = new Car();
            car.setManufactureId(Integer.parseInt(req.getParameter("Car_Brand")));
            car.setModel(req.getParameter("Model"));
            car.setPower(Integer.parseInt(req.getParameter("Power")));
            car.setRangeKm(Integer.parseInt(req.getParameter("Range")));
            car.setPrice(Integer.parseInt(req.getParameter("Price")));
            car.setQuantity(Integer.parseInt(req.getParameter("Quantity")));
            carDao.insert(car);
        } catch (Exception e) {
            throw new DAOException (e);
        }
    }
    protected void removeCar(HttpServletRequest req, HttpServletResponse resp) throws DAOException {
        try {
            MySQLCarDao carDao = (MySQLCarDao) req.getSession().getAttribute("SQLCar");
            Car car = new Car();
            car.setId(Integer.parseInt(req.getParameter("id")));
            carDao.delete(car);
        } catch (Exception e) {
            throw new DAOException (e);
        }
    }
    protected void toEditCar (HttpServletRequest req, HttpServletResponse resp) throws DAOException, ServletException, IOException {
        try {
            int id = Integer.parseInt(req.getParameter("id"));
            MySQLCarDao carDao = (MySQLCarDao) req.getSession().getAttribute("SQLCar");
            MySQLManufactureDao manuDao = (MySQLManufactureDao) req.getSession().getAttribute("SQLManufacture");
            Car car = carDao.read(id);
            Manufacture m = manuDao.read(car.getManufactureId());

            if (req.getParameter("Car To Edit") == null) {
                req.setAttribute("Car To Edit", car);
            } else {
                req.removeAttribute("Car To Edit");
                req.setAttribute("Car To Edit", car);
            }
            if (req.getParameter("Car_Brand") == null) {
                req.setAttribute("Car_Brand", m);
            } else {
                req.removeAttribute("Car_Brand");
                req.setAttribute("Car_Brand", car);
            }
        } catch (Exception e) {
            throw new DAOException(e);
        }
        if (req.getRequestDispatcher("/jsp/CarEditPage.jsp") != null) {
            req.getRequestDispatcher("/jsp/CarEditPage.jsp").forward(req, resp);
        }
    }
    protected void editCar(HttpServletRequest req, HttpServletResponse resp) throws DAOException {
        try {
            MySQLCarDao carDao = (MySQLCarDao) req.getSession().getAttribute("SQLCar");
            MySQLManufactureDao manufactureDao = (MySQLManufactureDao) req.getSession().getAttribute("SQLManufacture");
            Car car = new Car();
            List<Manufacture> l = new ArrayList<>();
            l = manufactureDao.getAll();
            for(Manufacture m: l){
                String cb = m.getCarBrand();
                if(cb.equals(req.getParameter("Car_Brand"))){
                    car.setManufactureId(m.getId());
                }
            }
            car.setModel(req.getParameter("Model"));
            car.setPower(Integer.parseInt(req.getParameter("Power")));
            car.setRangeKm(Integer.parseInt(req.getParameter("Range")));
            car.setPrice(Integer.parseInt(req.getParameter("Price")));
            car.setQuantity(Integer.parseInt(req.getParameter("Quantity")));
            car.setId(Integer.parseInt(req.getParameter("id")));
            carDao.update(car);
        } catch (Exception e) {
            throw new DAOException (e);
        }
    }
    protected List getListCar (HttpServletRequest req, HttpServletResponse resp) throws DAOException {
        List<Car> list;
        try {
            MySQLCarDao carDao = (MySQLCarDao) req.getSession().getAttribute("SQLCar");
            list = carDao.getAll();
        } catch (Exception e) {
            throw new DAOException (e);
        } return list;
    }
    protected void fillFormCars (HttpServletRequest req, HttpServletResponse resp) throws DAOException, ServletException, IOException {
        try {
            if (req.getParameter("CarList") == null) {
                List<Car> list = new ArrayList();
                list = getListCar(req, resp);
                req.setAttribute("CarList", list);
            }
                if (req.getParameter("ManufactureList") == null) {
                    List<Manufacture> list = new ArrayList();
                    list = getListManufacture(req, resp);
                    req.setAttribute("ManufactureList", list);
                }

        } catch (Exception e) {
            throw new DAOException(e);
        }
        if (req.getRequestDispatcher("/jsp/Car.jsp") != null) {
            req.getRequestDispatcher("/jsp/Car.jsp").forward(req, resp);
        }
    }
    protected String stringToMD5(String st) throws ServletException, UnsupportedEncodingException{
        byte[] digest;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.reset();
            md.update(st.getBytes());
            digest = md.digest();

        } catch (Exception e) {
            throw new ServletException(e);
        }
        BigInteger bigInt = new BigInteger(1,digest);
        String md5Hex = bigInt.toString(16);

        while( md5Hex.length() < 32 ){
            md5Hex = "0" + md5Hex;
        }

        return md5Hex;
    }

    protected void addUser(HttpServletRequest req, HttpServletResponse resp) throws DAOException {
        try {
            Users user = new Users();
            String loginJSP = req.getParameter("loginJSP");
            String passJSP = req.getParameter("passJSP");
            String loginMD5 = stringToMD5(loginJSP);
            String passMD5 = stringToMD5(passJSP);
            user.setUser(loginMD5);
            user.setPassword(passMD5);
            MySQLUsersDao usersDao = (MySQLUsersDao) req.getSession().getAttribute("SQLUsers");
            usersDao.insert(user);
        } catch (Exception e) {
            throw new DAOException (e);
        }

    }
    protected void removeUser(HttpServletRequest req, HttpServletResponse resp) throws DAOException {
        try {
            Users user = new Users();
            user.setId(Integer.parseInt(req.getParameter("id")));
            MySQLUsersDao usersDao = (MySQLUsersDao) req.getSession().getAttribute("SQLUsers");
            usersDao.delete(user);
        } catch (Exception e) {
            throw new DAOException (e);
        }

    }

    protected List getUserslist (HttpServletRequest req, HttpServletResponse resp) throws DAOException, ServletException, IOException {
        List<Users> list;
        try {
            MySQLUsersDao usersDao = (MySQLUsersDao) req.getSession().getAttribute("SQLUsers");
            list = usersDao.getAll();
        } catch (Exception e) {
            throw new DAOException(e);
        }
        return list;
    }
    protected void showUsers (HttpServletRequest req, HttpServletResponse resp) throws DAOException, ServletException, IOException {
        try {
            if (req.getParameter("UsersList") == null) {
                List<Users> list = new ArrayList();
                list = getUserslist(req, resp);
                req.setAttribute("UsersList", list);
            }
        } catch (Exception e) {
            throw new DAOException(e);
        }
        if (req.getRequestDispatcher("/jsp/Users.jsp") != null) {
            req.getRequestDispatcher("/jsp/Users.jsp").forward(req, resp);
        }
    }

    protected void checkUsers (HttpServletRequest req, HttpServletResponse resp) throws DAOException, ServletException, IOException {
        try {
                List<Users> list = new ArrayList();
                list = getUserslist(req, resp);

            String loginJSP = req.getParameter("loginJSP");
            String passJSP = req.getParameter("passJSP");
            String loginMD5 = stringToMD5(loginJSP);
            String passMD5 = stringToMD5(passJSP);
            for (Users user:list){
                if (req.getSession().getAttribute("Login")==null){
                    String login = user.getUser();
                    String pass = user.getPassword();
                    if (loginMD5.equals(login)&&passMD5.equals(pass)){
                        req.getSession().setAttribute("Login","ok");
                    }
                }
            }
        } catch (Exception e) {
            throw new DAOException(e);
        }
        if(req.getSession().getAttribute("Login")==null){
            if (req.getRequestDispatcher("/jsp/Applogin.jsp") != null) {
                req.getRequestDispatcher("/jsp/Applogin.jsp").forward(req, resp);
            }
        } else {
            if (req.getRequestDispatcher("/jsp/MainPage.jsp") != null) {
                req.getRequestDispatcher("/jsp/MainPage.jsp").forward(req, resp);
            }
        }
    }



        protected void processRequest(HttpServletRequest req, HttpServletResponse resp)
                throws ServletException, IOException {
            resp.setContentType("text/html;charset=utf-8");
            String action = req.getParameter("action");
            if (action.equals("Applogin")){
                try{
                    checkUsers(req, resp);
                }catch (DAOException e){
                    throw new ServletException(e);
                }
            } else if (req.getSession().getAttribute("Login") == null){
                if (req.getRequestDispatcher("/jsp/Applogin.jsp") != null) {
                    req.getRequestDispatcher("/jsp/Applogin.jsp").forward(req, resp);
                }
            } else if(action.equals("To MainPage")) {
                     req.getRequestDispatcher("/jsp/MainPage.jsp").forward(req, resp);
                 } else if (action.equals("To Users")) {
                     try{
                         showUsers(req, resp);
                     }catch (DAOException e) {
                         throw new ServletException(e);
                     }
                }  else if (action.equals("Add Manufacture")) {
                try {
                    if (checkInputManufacture(req, resp)) {
                        addManufacture(req, resp);
                        showManufactures(req, resp);
                    } else {
                        req.setAttribute("Wrong input data", "Введите данные");
                        showManufactures(req, resp);
                    }
                } catch (DAOException e) {
                    throw new ServletException(e);
                }
            } else if (action.equals("Add CarOrder")) {
                try {
                    if(checkInputCarOrder(req, resp)){
                        addCarOrder(req, resp);
                        fillFormCarOrders(req, resp);
                    }else{
                        req.setAttribute("Wrong input data", "Введите данные");
                        fillFormCarOrders(req, resp);
                    }
                } catch (DAOException e) {
                    throw new ServletException(e);
                }
            } else if (action.equals("Add User")) {
                try {
                    if(checkInputUser(req, resp)){
                    addUser(req, resp);
                    showUsers(req, resp);
                    }else{
                        req.setAttribute("Wrong input data", "Введите данные");
                        showUsers(req, resp);
                    }
                } catch (DAOException e) {
                    throw new ServletException(e);
                }
            }  else if (action.equals("Add Car")) {
                try {
                    if(checkInputCar(req, resp)){
                        addCar(req, resp);
                        fillFormCars(req, resp);
                    } else {
                        req.setAttribute("Wrong input data", "Введите число");
                        fillFormCars(req, resp);
                    }
                } catch (DAOException e) {
                    throw new ServletException(e);
                }
            } else if (action.equals("Remove User")) {
                try {
                    removeUser(req, resp);
                    showUsers(req, resp);
                } catch (DAOException e) {
                    throw new ServletException(e);
                }
            } else if (action.equals("Remove Manufacture")) {
                try {
                    removeManufacture(req, resp);
                    showManufactures(req, resp);
                } catch (DAOException e) {
                    throw new ServletException(e);
                }
            } else if (action.equals("Remove CarOrder")) {
                try {
                    removeCarOrder(req, resp);
                    fillFormCarOrders(req, resp);
                } catch (DAOException e) {
                    throw new ServletException(e);
                }
            } else if (action.equals("Remove Car")) {
                try {
                    removeCar(req, resp);
                    fillFormCars(req, resp);
                } catch (DAOException e) {
                    throw new ServletException(e);
                }
            } else if (action.equals("Show Manufactures")) {
                    try {
                        showManufactures(req, resp);
                    } catch (DAOException e) {
                        throw new ServletException(e);
                    }
                } else if (action.equals("Show Cars")) {
                    try {
                        fillFormCars(req, resp);
                    } catch (DAOException e) {
                        throw new ServletException(e);
                    }
                } else if (action.equals("Show Car Orders")) {
                    try {
                        fillFormCarOrders(req, resp);
                    } catch (DAOException e) {
                        throw new ServletException(e);
                    }
                } else if (action.equals("Edit Car")) {
                    try {
                        if(checkInputEditCar(req, resp)){
                        editCar(req, resp);
                        fillFormCars(req, resp);
                        }else{
                            req.setAttribute("Wrong input data", "Введите данные");
                            if (req.getRequestDispatcher("/jsp/CarEditPage.jsp") != null) {
                                req.getRequestDispatcher("/jsp/CarEditPage.jsp").forward(req, resp);
                            }

                        }
                    } catch (DAOException e) {
                        throw new ServletException(e);
                    }
                } else if (action.equals("GoToEditCar")) {
                    try {
                        toEditCar(req, resp);
                    } catch (DAOException e) {
                        throw new ServletException(e);
                    }
                }
            }


        public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            processRequest(req,resp);
        }
        public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            processRequest(req, resp);
        }
    }


