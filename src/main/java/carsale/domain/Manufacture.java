package carsale.domain;

import java.io.Serializable;

/**
 * Created by Busik on 12/30/16.
 */
public class Manufacture implements Serializable {
    private Integer id;
    private String carBrand;

    public Integer getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }
    public String getCarBrand() {
        return carBrand;
    }
    public Manufacture(){

    }
}

