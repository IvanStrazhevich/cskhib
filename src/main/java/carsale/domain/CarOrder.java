package carsale.domain;

import java.io.Serializable;

/**
 * Created by Busik on 1/16/17.
 */
public class CarOrder implements Serializable{
    private Integer id;
    private Integer carId;
    private Integer manufactureId;
    private String firstName;
    private String secondName;
    private String carBrand;
    private String model;


    public void setId(int id) {
        this.id = id;
    }
    public Integer getId() {
        return id;
    }
    public void setCarId(int carid) {
        this.carId = carid;
    }
    public Integer getCarId() {
        return carId;
    }
    public Integer getManufactureId() {
        return manufactureId;
    }
    public void setManufactureId(int manufactureId) {
        this.manufactureId = manufactureId;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getSecondName() {
        return secondName;
    }
    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }
    public String getCarBrand() {
        return carBrand;
    }
    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
}
