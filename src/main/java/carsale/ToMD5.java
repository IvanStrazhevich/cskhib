package carsale;

import javax.servlet.ServletException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;

/**
 * Created by Busik on 4/10/17.
 */
public class ToMD5 {
    public String string;

    public ToMD5(String string){
        this.string = string;
    }

    public String convertToMD5()  {


        byte[] digest = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.reset();
            md.update(string.getBytes());
            digest = md.digest();

        } catch (Exception e) {
            e.printStackTrace();
        }
        BigInteger bigInt = new BigInteger(1,digest);
        String md5Hex = bigInt.toString(16);

        while( md5Hex.length() < 32 ){
            md5Hex = "0" + md5Hex;
        }

       return md5Hex;
    }
}
