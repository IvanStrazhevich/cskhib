package carsale.sql;

import carsale.dao.CarDao;
import carsale.dao.DAOException;
import carsale.domain.Car;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Busik on 1/5/17.
 */
public class MySQLCarDao implements CarDao{
    private final Connection connection;
    private PreparedStatement readpstm = null;
    private PreparedStatement deletepstm = null;
    private PreparedStatement updatepstm = null;
    private PreparedStatement getAllpstm = null;
    private PreparedStatement insertpstm = null;

    protected  void createInsertStatement() throws SQLException {
        String sql = "insert into Car (Manufacture_ID, Model, Power, Range_Km, Price, Quantity) values (?,?,?,?,?,?);";
        try {
            insertpstm = connection.prepareStatement(sql);
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    protected void executeInsertStatement(Car car) throws SQLException {
        try {
            insertpstm.setInt(1, car.getManufactureId());
            insertpstm.setString(2, car.getModel());
            insertpstm.setInt(3, car.getPower());
            insertpstm.setInt(4, car.getRangeKm());
            insertpstm.setInt(5, car.getPrice());
            insertpstm.setInt(6, car.getQuantity());
            int count = insertpstm.executeUpdate();
            if (count == 0) {
                throw new SQLException();
            }
        } catch (Exception e) {
            throw new SQLException(e);
        }
    }

    protected void createDeleteStatement() throws SQLException {
        String sql = "DELETE FROM car WHERE id= ?;";
        try {
            deletepstm = connection.prepareStatement(sql);
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    protected void executeDeleteStatement(Car car) throws SQLException {
        try {
            deletepstm.setInt(1,car.getId());
            int count = deletepstm.executeUpdate();
            if (count == 0) {
                throw new SQLException();
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    protected  void createUpdateStatement() throws SQLException {
        String sql = "UPDATE car SET manufacture_id =?, model = ?, power = ?, range_km =?," +
                " price =?, quantity = ? WHERE ID = ?;";
        try {
            updatepstm = connection.prepareStatement(sql);
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    protected void executeUpdateStatement(Car car) throws SQLException {
        try {
            updatepstm.setInt(1, car.getManufactureId());
            updatepstm.setString(2, car.getModel());
            updatepstm.setInt(3, car.getPower());
            updatepstm.setInt(4, car.getRangeKm());
            updatepstm.setInt(5, car.getPrice());
            updatepstm.setInt(6, car.getQuantity());
            updatepstm.setInt(7, car.getId());

            int count = updatepstm.executeUpdate();
            if (count == 0) {
                throw new SQLException();
            }
        } catch (Exception e) {
            throw new SQLException(e);
        }
    }

    protected void createReadStatement() throws SQLException {
        String sql = "SELECT ID, MODEL, MANUFACTURE_ID," +
        "POWER, RANGE_KM, PRICE, QUANTITY FROM CAR WHERE id = ?;";
        try {
            readpstm = connection.prepareStatement(sql);
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    protected Car executeReadStatement(int key) throws SQLException {
        Car c = new Car();
        try {
            readpstm.setInt(1, key);
            ResultSet rs = readpstm.executeQuery();
            rs.next();
            c.setId(rs.getInt("ID"));
            c.setManufactureId(rs.getInt("manufacture_id"));
            c.setModel(rs.getString("model"));
            c.setPower(rs.getInt("power"));
            c.setRangeKm(rs.getInt("range_km"));
            c.setPrice(rs.getInt("price"));
            c.setQuantity(rs.getInt("quantity"));
        } catch (SQLException e) {
            throw new SQLException(e);
        }
        return c;
    }

    protected void createGetAllStatement() throws SQLException {
        try{
            String sql = "SELECT C.ID, C.MODEL, C.MANUFACTURE_ID," +
                    "C.POWER, C.RANGE_KM, C.PRICE, C.QUANTITY, M.Car_Brand FROM CAR C LEFT OUTER JOIN Manufacture M on C.Manufacture_ID = M.ID;";
        getAllpstm = connection.prepareStatement(sql);
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    protected List executeGetAllStatement() throws SQLException {
        List<Car> list = new ArrayList<>();
        try {
            ResultSet rs = getAllpstm.executeQuery();
            while (rs.next()) {
                Car c = new Car();
                c.setId(rs.getInt("ID"));
                c.setModel(rs.getString("MODEL"));
                c.setManufactureId(rs.getInt("manufacture_id"));
                c.setModel(rs.getString("model"));
                c.setPower(rs.getInt("power"));
                c.setRangeKm(rs.getInt("range_km"));
                c.setPrice(rs.getInt("price"));
                c.setQuantity(rs.getInt("quantity"));
                c.setCarBrand(rs.getString("car_brand"));
                list.add(c);
            }
        }catch (SQLException e) {
            throw new SQLException(e);
        }    return list;
    }


    @Override
    public Car read(int key) throws DAOException {
        try {
            if (readpstm == null) {
                createReadStatement();
            }

            return executeReadStatement(key);
        }catch (SQLException e){
            throw new DAOException("fail on read");
        }
    }

    @Override
    public void update(Car car) throws DAOException {
        try {
            if (updatepstm == null) {
                createUpdateStatement();
            }
            executeUpdateStatement(car);
        } catch (SQLException e) {
            throw new DAOException("fail on update");
        }
    }

    @Override
    public void delete(Car car) throws DAOException {
        try {
            if (deletepstm == null) {
                createDeleteStatement();
            }
            executeDeleteStatement(car);
        } catch (SQLException e) {
            throw new DAOException("fail on delete");
        }
    }


    @Override
    public List<Car> getAll() throws DAOException {
        List<Car> list;
        try {
            if (getAllpstm == null) {
                createGetAllStatement();
            }
            list = executeGetAllStatement();

            return list;
        }catch (SQLException e){
            throw new DAOException("fail on getAll");
        }
    }
    @Override
    public void insert(Car car) throws DAOException {
        try {
            if (insertpstm == null) {
                createInsertStatement();
            }
            executeInsertStatement(car);
        } catch (SQLException e) {
            throw new DAOException("fail on insert");
        }
    }

    @Override
    public void close() throws DAOException {
        if (readpstm != null) {
            try {
                readpstm.close();
            } catch (SQLException e) {
                throw new DAOException("Fail on close");
            }
        }
        if (deletepstm != null) {
            try {
                deletepstm.close();
            } catch (SQLException e) {
                throw new DAOException("Fail on close");
            }
        }
        if (updatepstm != null) {
            try {
                updatepstm.close();
            } catch (SQLException e) {
                throw new DAOException("Fail on close");
            }
        }
        if (getAllpstm != null) {
            try {
                getAllpstm.close();
            } catch (SQLException e) {
                throw new DAOException("Fail on close");
            }
        }
    }

    public MySQLCarDao(Connection connection) {
        this.connection = connection;
    }
}

