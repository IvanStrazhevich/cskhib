package carsale.sql;


import carsale.dao.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;



/**
 * Created by Busik on 12/29/16.
 */
public class MySQLDaoFactory implements DaoFactory {
    public Connection getTestSQLConnection() throws DAOException {
        String user;
        String password;
        String url;
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Properties properties = new Properties();
            properties.load(getClass().getClassLoader().getResourceAsStream("connectiontest.properties"));
            url = properties.getProperty("url","");
            user = properties.getProperty("user","");
            password = properties.getProperty("password","");
            connection = DriverManager.getConnection(url, user, password);
        }catch (Exception e) {
            throw new DAOException("fail on testconnection");
        }
        return connection;
    }
    public Connection getSQLConnection() throws DAOException {
        String user;
        String password;
        String url;
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Properties properties = new Properties();
            properties.load(getClass().getClassLoader().getResourceAsStream("connection.properties"));
            url = properties.getProperty("url","");
            user = properties.getProperty("user","");
            password = properties.getProperty("password","");
            connection = DriverManager.getConnection(url, user, password);
        }catch (Exception e) {
            throw new DAOException("fail on connection");
        }
        return connection;
    }
    @Override
    public UsersDao getUsersDao() throws DAOException {
        Connection con = getSQLConnection();
        UsersDao usersDao = new MySQLUsersDao(con);
        return usersDao;
    }
    @Override
    public CarDao getCarDao() throws DAOException {
        Connection con = getSQLConnection();
        CarDao carDao = new MySQLCarDao(con);
        return carDao;
    }

    @Override
    public ManufactureDao getManufactureDao() throws DAOException{
        Connection con = getSQLConnection();
        ManufactureDao manufactureDao = new MySQLManufactureDao(con);
        return manufactureDao;
    }

    @Override
    public CarOrderDao getCarOrderDao() throws DAOException{
        Connection con = getSQLConnection();
        CarOrderDao carOrderDao = new MySQLCarOrderDao(con);
        return carOrderDao;
    }


    public MySQLDaoFactory() throws DAOException {
        String driver = "com.mysql.jdbc.Driver";
        try {
            Class.forName(driver);//Регистрируем драйвер
        } catch (ClassNotFoundException e) {
            throw new DAOException("fail on connection");
        }
    }
}







