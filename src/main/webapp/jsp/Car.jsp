<%@ page contentType = "text/html;charset=utf-8" %>
<%@ page import="javax.servlet.http.*" %>
<%@ page import="java.util.*" %>
<%@ page import="carsale.domain.Car" %>
<%@ page import="carsale.domain.Manufacture" %>
<html>
<head>
    <title>
        CarSaleKitSample
    </title>
    <style>
        h1{

            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: slateblue;

        }
        <% if(request.getAttribute("Wrong input data")!=null)
        out.println("input[type=\"text\"]::-webkit-input-placeholder {"+
            "color: red;"+
        "}"+
        "input[type=\"text\"]::-moz-placeholder {"+
            "color: red;"+
        "}"
        );%>
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: mediumslateblue;
        }

        li {
            float: left;
        }

        li a {
            display: block;
            color: white;
            text-align: center;
            padding: 16px;
            text-decoration: none;
        }

        li a:hover {
            background-color: darkslateblue;
        }
    </style>

</head>

<body>
<div style="background-color: slateblue" >
    <h1>

        <ul>
            <li><a href="CarOrder?action=Show+Car+Orders">Car Orders</a></li>
            <li><a href="Manufacture?action=Show+Manufactures"  >Manufactures</a></li>
            <li><a href="Car?action=Show+Cars" style="background-color: slateblue;">Cars</a></li>
        </ul>

    </h1>
    <h1>
        <table>
            <tr>
                <th> Id </th>
                <th>Model</th>
                <th>Car Brand</th>
                <th>Power kw</th>
                <th>Range km</th>
                <th>Price $</th>
                <th>Quantity</th>


            </tr>
            <% for (Car c : (List<Car>)request.getAttribute("CarList")) {
                out.println("<tr>"+
                        "<form action=\"Car\">"+
                        "<td>" + c.getId() + "</td>"+
                        "<td>" + c.getModel() + "</td>"+
                        "<td>" + c.getCarBrand() + "</td>"+
                        "<td>" + c.getPower() + "</td>"+
                        "<td>" + c.getRangeKm() + "</td>"+
                        "<td>" + c.getPrice() + "</td>"+
                        "<td>" + c.getQuantity() + "</td>"+
                        "<input type=\"hidden\"name=\"id\"value=\""+c.getId()+"\">"+
                        "<input type=\"hidden\"name=\"action\"value=\"Remove Car\">" +
                        "<td><input type=\"submit\" style=\"color: darkslateblue\" value=\"Remove\"> </td>" +
                        "<td><a style=\"color: darkslateblue\" href =\"CarEditPage?action=GoToEditCar&id="+c.getId()+"\">Edit</a></td>" +
                        "</form>" +
                        "</tr>");
            }%>

        </table>
    </h1>
    <form action="Car">
        <input type="text" size="30"name ="Model" placeholder="enter Model">
        <select style="color: darkslateblue" name="Car_Brand">
            <% for (Manufacture m : (List<Manufacture>)request.getAttribute("ManufactureList")) {
                out.println("<option  value=\"" +m.getId()+ "\">" + m.getCarBrand() +" </option>"
                );
            }%>
        </select>
        <input type="text" size="30" name ="Power"  placeholder="enter Power kWt <% if(request.getAttribute("Wrong input data")!=null){out.println( request.getAttribute("Wrong input data"));}%> "> <br>
        <input type="text" size="30" name ="Range"  placeholder="enter Range_km <% if(request.getAttribute("Wrong input data")!=null){out.println( request.getAttribute("Wrong input data"));}%> ">
        <input type="text" size="30" name ="Price"  placeholder="enter Price $ <% if(request.getAttribute("Wrong input data")!=null){out.println( request.getAttribute("Wrong input data"));}%> ">
        <input type="text" size="30" name ="Quantity" placeholder="enter Quantity <% if(request.getAttribute("Wrong input data")!=null){out.println( request.getAttribute("Wrong input data"));}%> ">
        <input type="hidden"  name ="action" value="Add Car">
        <input type="submit" style="color: darkslateblue" value="Add Car">
    </form>
    <form action="MainPage">
        <input type="hidden" name ="action" value="To MainPage">
        <input type="submit" style="color: darkslateblue" value="To Main Page">
    </form>
</div>
</body>
</html>