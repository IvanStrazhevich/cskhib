<%@ page contentType = "text/html;charset=utf-8" %>
<%@ page import="javax.servlet.http.*" %>
<%@ page import="java.util.*" %>
<%@ page import="carsale.domain.Users" %>
<html>
<head>
    <style>
        h1{

            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: slateblue;

        }
        <% if(request.getAttribute("Wrong input data")!=null)
        out.println("input[type=\"text\"]::-webkit-input-placeholder {"+
            "color: red;"+
        "}"+
        "input[type=\"text\"]::-moz-placeholder {"+
            "color: red;"+
        "}" +
        "input[type=\"password\"]::-webkit-input-placeholder {"+
            "color: red;"+
        "}"+
        "input[type=\"password\"]::-moz-placeholder {"+
            "color: red;"+
        "}"
        );%>
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: mediumslateblue;
        }

        li {
            float: left;
        }

        li a {
            display: block;
            color: white;
            text-align: center;
            padding: 16px;
            text-decoration: none;
        }

        li a:hover {
            background-color: darkslateblue;
        }
    </style>
    <title>
        CarSaleKitSample
    </title>
</head>

<body>
<h1>
    Car Sale Kit Users
</h1>
<h1>
<table>

    <tr>
        <th>ID</th>
        <th>User</th>
        <th>Password</th>
    </tr>
     <% for (Users u: (List<Users>)request.getAttribute("UsersList")) {
                out.println("<tr>" +
                        "<form action=\"Users\">"+
                                "<td>"+u.getId()+"</td>"+
                                "<td>"+u.getUser()+"</td>"+
                                "<td >"+u.getPassword()+"</td>"+
                                "<input type=\"hidden\"name=\"id\"value=\""+u.getId()+"\">"+
                                "<input type=\"hidden\"name=\"action\"value=\"Remove User\">" +
                                "<td><input type=\"submit\" style=\"color: darkslateblue\" value=\"Remove\"> </td>" +
                        "</form>" +
                "</tr>");
                }%>
</table>

</h1>
<h1>
<form action="Users">
    <input type="hidden" name ="action" value="Add User">
    <input type="text" size = "30" name ="loginJSP" placeholder="insert your login <% if(request.getAttribute("Wrong input data")!=null){out.println( request.getAttribute("Wrong input data"));}%>">
    <input type="password" size = "30" name ="passJSP" placeholder="insert your password <% if(request.getAttribute("Wrong input data")!=null){out.println( request.getAttribute("Wrong input data"));}%>">
    <input type="submit" style="color: darkslateblue" value="Add User">
</form>

<form action="MainPage">
    <input type="hidden" name ="action" value="To Users">
    <input type="submit" value="To Users">
</form>
<form action="Manufacture">
    <input type="hidden" name ="action" value="Show Manufactures">
    <input type="submit" value="Show Manufactures">
</form>
<form action="Car">
    <input type="hidden" name ="action" value="Show Cars">
    <input type="submit" value="Show Cars">
</form>
<form action="CarOrder">
    <input type="hidden" name ="action" value="Show Car Orders">
    <input type="submit" value="Show Car Orders">
</form>
    </h1>
</body>
</html>
