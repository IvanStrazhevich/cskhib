<%@ page contentType = "text/html;charset=utf-8" %>
<%@ page import="javax.servlet.http.*" %>
<%@ page import="java.util.*" %>
<%@ page import="carsale.domain.CarOrder" %>
<%@ page import="carsale.domain.Car" %>
<html>
    <head>
        <title>
            CarSaleKitSample
        </title>
        <style>
            h1{

                list-style-type: none;
                margin: 0;
                padding: 0;
                overflow: hidden;
                background-color: slateblue;

            }
            <% if(request.getAttribute("Wrong input data")!=null)
        out.println("input[type=\"text\"]::-webkit-input-placeholder {"+
            "color: red;"+
        "}"+
        "input[type=\"text\"]::-moz-placeholder {"+
            "color: red;"+
        "}"
        );%>
            ul {
                list-style-type: none;
                margin: 0;
                padding: 0;
                overflow: hidden;
                background-color: mediumslateblue;
            }

            li {
                float: left;
            }

            li a {
                display: block;
                color: white;
                text-align: center;
                padding: 16px;
                text-decoration: none;
            }

            li a:hover {
                background-color: darkslateblue;
            }
        </style>
    </head>
 
    <body>
    <div style="background-color: slateblue" >
<h1>
    <ul>
        <li><a href="CarOrder?action=Show+Car+Orders" style="background-color: slateblue;">Car Orders</a></li>
        <li><a href="Manufacture?action=Show+Manufactures"  >Manufactures</a></li>
        <li><a href="Car?action=Show+Cars" >Cars</a></li>
    </ul>
</h1>
<h1> 

    <form action="CarOrder">
        <input type="text" size="30" name ="First_Name" placeholder="enter First Name <% if(request.getAttribute("Wrong input data")!=null){out.println( request.getAttribute("Wrong input data"));}%>">

            <select style="color: darkslateblue" name="id">
    <% for (Car c : (List<Car>)request.getAttribute("CarList")) {
    out.println("<option  value=\"" +c.getId()+ "\">" +c.getModel()+" "+ c.getCarBrand() +" "+ c.getPrice() +" </option>"
           );
    }%>
            </select><br>
        <input type="text" size="30" name ="Second_Name" placeholder="enter Second Name <% if(request.getAttribute("Wrong input data")!=null){out.println( request.getAttribute("Wrong input data"));}%>">
        <input type="hidden"  name ="action" value="Add CarOrder">


        <input type="submit" style="color: darkslateblue" value="AddOrder">
    </form>
    <table>
        <tr>
            <th> Id </th>
            <th>First Name</th>
            <th>Second Name</th>
            <th>Car Brand</th>
            <th>Model</th>
        </tr>
        <% for (CarOrder co: (List<CarOrder>)request.getAttribute("CarOrderList")) {

            out.println("<tr>"+
                    "<form action=\"CarOrder\">"+
                    "<td>" + co.getId() + "</td>"+
                    "<td>" + co.getFirstName() + "</td>"+
                    "<td>" + co.getSecondName() + "</td>"+
                    "<td>" + co.getCarBrand() + "</td>"+
                    "<td>" + co.getModel() + "</td>"+
                    "<input type=\"hidden\"name=\"id\"value=\""+co.getId()+"\">"+
                    "<input type=\"hidden\"name=\"action\"value=\"Remove CarOrder\">" +
                    "<td><input type=\"submit\" style=\" color: darkslateblue\" value=\"Remove\"> </td>" +
                    "</form>" +
                    "</tr>");
        }%>
    </table>
</h1>
<form action="MainPage">
<input type="hidden" name ="action" value="To MainPage">
  <input type="submit" style=" color: darkslateblue"value="To Main Page">
</form>
    </div>
    </body>
</html>

